$(document).ready(function() {

    var zi = 1;
    var emptySquare = 9;
    $.fn.extend({
        puzzle: function(square_size) {
            var gameObjectElement = '#' + $(this).attr('id');            
            var boardSize = (square_size * 3) + 'px';
            $(gameObjectElement).html('<div id="board"></div>');
            $('#board').css({
                position: 'absolute',
                width: boardSize,
                height: boardSize,
                border: '1px solid gray'
            });

            var arrNum = [];
            for (var i = 0; i < 9; i++) {
                arrNum.push(i);
            }


            /****************************
             * COMENTAR ESTO PARA PROBAR*
             * **************************/
            arrNum.sort(function() {
                return 0.8 - Math.random();
            });
            /****************************/

            var arrPos = [];

            $.each(arrNum, function(iNum, valor) {

                var left = (iNum % 3) * square_size;
                var top = Math.floor(iNum / 3) * square_size;

                arrPos.push({left: left, top: top});

                $('#board').append("<div style='" +
                        "left: " + ((iNum % 3) * square_size) + "px; " +
                        "top: " + Math.floor(iNum / 3) * square_size + "px; " +
                        "width: " + square_size + "px; " +
                        "height: " + square_size + "px; " +
                        "background-position: " + (-(valor % 3) * square_size) + "px " + -Math.floor(valor / 3) * square_size + "px " +
                        "' data-position=" + valor + "></div>");
            });

            var sons = $('#board').children();
            var $_lastSonPos;

            $.each(sons, function(iSon, sVal) {
                var lastSonPos = $(sVal).data('position');
                if (lastSonPos === 8) {
                    $_lastSonPos = $(sVal);
                    $(sVal).css({backgroundImage: "", background: "#ffffff"});
                }
            });

            switchBlankSpace($_lastSonPos);

            $('#board').children('div').click(function() {
                Move(this, square_size);
            });

            $('#btn-aceptar').click(function(e) {
                concursar(arrPos);
            });
        }
    });

    function switchBlankSpace($_lastSonPos) {
        var son = $('#board').children("div:nth-child(" + emptySquare + ")");
        var son_left = $(son).css('left');
        var son_top = $(son).css('top');
        var oldPosLeft = $($_lastSonPos).css('left');
        var oldPosTop = $($_lastSonPos).css('top');

        $($_lastSonPos).attr('id', 'lastChild');
        $($_lastSonPos).css('left', son_left);
        $($_lastSonPos).css('top', son_top);
        $(son).css('left', oldPosLeft);
        $(son).css('top', oldPosTop);

    }

    function Move(clicked_square, square_size) {

        var movable = false;

        var oldx = $('#lastChild').css('left');
        var oldy = $('#lastChild').css('top');

        var newx = $(clicked_square).css('left');
        var newy = $(clicked_square).css('top');
        if (oldx === newx && newy === (parseInt(oldy) - square_size) + 'px')
            movable = true;
        if (oldx === newx && newy === (parseInt(oldy) + square_size) + 'px')
            movable = true;
        if ((parseInt(oldx) - square_size) + 'px' === newx && newy === oldy)
            movable = true;
        if ((parseInt(oldx) + square_size) + 'px' === newx && newy === oldy)
            movable = true;
        if (movable) {
            $(clicked_square).css('z-index', zi++);
            $(clicked_square).animate({left: oldx, top: oldy}, 200, function() {
                $('#lastChild').css('left', newx);
                $('#lastChild').css('top', newy);
            });
        }
    }

    function concursar() {

        var win = new Array();

        var arrNewDivs = $('#board').children();

        $.each(arrNewDivs, function(ind, val) {
            var pos = $(val).data('position');

            var left = $(val).css('left');
            var top = $(val).css('top');

            var lpos = left.indexOf('px');
            var left = parseInt(left.substring(0, lpos));

            var tpos = top.indexOf('px');
            var top = parseInt(top.substring(0, tpos));

            if (pos === 0) {
                if (left === 0 && top === 0) {                  
                    win.push(true);
                } else {                   
                    win.push(false);
                }
            } else if (pos === 1) {
                if (left === 180 && top === 0) {                    
                    win.push(true);
                } else {                  
                    win.push(false);
                }

            } else if (pos === 2) {
                if (left === 360 && top === 0) {                  
                    win.push(true);
                } else {                  
                    win.push(false);
                }
            } else if (pos === 3) {
                if (left === 0 && top === 180) {
                    win.push(true);
                } else {
                    win.push(false);
                }
            } else if (pos === 4) {
                if (left === 180 && top === 180) {
                    win.push(true);
                } else {
                    win.push(false);
                }
            } else if (pos === 5) {
                if (left === 360 && top === 180) {
                    win.push(true);
                } else {
                    win.push(false);
                }
            } else if (pos === 6) {
                if (left === 0 && top === 360) {
                    win.push(true);
                } else {
                    win.push(false);
                }
            } else if (pos === 7) {
                if (left === 180 && top === 360) {
                    win.push(true);
                } else {
                    win.push(false);
                }
            } else if (pos === 8) {
                if (left === 360 && top === 360) {
                    win.push(true);
                } else {
                    win.push(false);
                }
            }
        });

        var qty_bool = 0;

        $.each(win, function(iWin, vWin) {
            if (vWin === true) {
                qty_bool++;
            }
        });

        if (qty_bool === win.length) {
            alert('EUREKA!!!');
        }

    }

    $('#game_object').puzzle(180);
});
